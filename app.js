/**
 * Created by PiaseckiJ on 11/04/2017.
 */
var express = require('express');
var cors = require('cors');

var app = express();
app.use(cors());
var port = process.env.PORT ||8000;

var simCount = 9;
var requestsCounter = 0;
var simulations = [];

var genSimulations = function(){

    setInterval(function (){

        var simNo = Math.floor(Math.random() * simCount) + 1;

        requestsCounter++;
        simulations.push(
            {
                "requestId": requestsCounter,
                "timestamp": Date.now(),
                "id": simNo,
                "name": "simulation"+simNo,
                "url": "publish"+simNo,
                "method": 'GET',
                "response": "hello from simulation "+ simNo +" - response "+ requestsCounter
            }
        );

        console.log('simulation ' + simNo + ' request ' + requestsCounter);

    }, 3000);
};

genSimulations();

app.get('/', function(req, res, next){

    console.log("--GET-- last reqest no "+ req.query.lastReq);

    var freshSims =[];

    for(var i = 0; i<simulations.length; i++){

        if(simulations[i].requestId>req.query.lastReq){
            freshSims.push(simulations[i]);
        }
    }
    if(freshSims.length>0){
        res.json({
            "simulations": freshSims
        });
    }

});

app.listen(port, function(){
    console.log('Running on PORT:' + port);
});