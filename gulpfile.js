/**
 * Created by PiaseckiJ on 11/04/2017.
 */
var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');

gulp.task('default', function(){
    nodemon({
        script: 'app.js',
        exit: 'js',
        ignore:['./node_modules/**']
    }).on('restart', function(){
        console.log('Restarting');
    });
});